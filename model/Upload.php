<?php

class Upload
{
    public static function image()
    {
        $targetDir = __DIR__ . "/../images/";
        $uploadOk = 0;
        $fileName = $_FILES["gambar"]["name"];
        $targetFile = $targetDir . basename($fileName);
        $fileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
        $hashName = md5(date('Y-m-d H:i:s') . $fileName) . "." . $fileType;
        $targetFile = $targetDir . $hashName;

        $check = getimagesize($_FILES["gambar"]["tmp_name"]);
        if ($check !== false) {
            echo "File is an image - " . $check["mime"] . "<br/>";
            $uploadOk = 1;
        } else {
            echo "File not an image<br/>";
        }

        if ($_FILES["gambar"]["size"] > 500000)
            echo "File is to large MAX 500KB<br/>";

        if ($fileType != "jpg" && $fileType != "png" && $fileType != "jpeg" && $fileType != "gif")
            echo "Sorry, only jpg png jpeg and gif accepted<br/>";

        if ($uploadOk === 0) {
            return false;
        }

        if (move_uploaded_file($_FILES["gambar"]["tmp_name"], $targetFile)) {
            return $hashName;
        } else
            return false;
    }
}
