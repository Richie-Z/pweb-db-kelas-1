<?php

class Koneksi
{
    private $host = "localhost";
    private $username = "root";
    private $password = "1133";
    private $dbName = "pweb_2022_kelas_1";
    private $port = "3306";
    public $koneksi;

    public function __construct()
    {
        $this->koneksi = mysqli_connect($this->host, $this->username, $this->password, $this->dbName, $this->port);
    }
}

if (!function_exists('dd')) {
    function dd($var)
    {
        echo '<pre>' . print_r($var, true) . '</pre>';
        exit;
    }
}
