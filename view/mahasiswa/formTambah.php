<div class="card">
    <div class="card-header">
        <h3>Tambah Mahasiswa</h3>
    </div>
    <div class="card-body">
        <form method="POST" action="view/mahasiswa/prosesTambah.php">

            <div class="form-group">
                <label>Nim </label>
                <input type="text" name="nim" class="form-control">
            </div>
            <div class="form-group">
                <label>Nama </label>
                <input type="text" name="nama" class="form-control">
            </div>
            <div class="form-group">
                <label>Tanggal Lahir </label>
                <input type="text" name="tgl_lahir" class="form-control">
            </div>
            <div class="form-group">
                <label>Alamat </label>
                <input type="text" name="alamat" class="form-control">
            </div>
            <div class="form-group">
                <label>Jenis Kelamin </label><br>
                <input type="radio" name="jenis_kelamin" value="L">Laki Laki
                <input type="radio" name="jenis_kelamin" value="P">Perempuan
            </div>
            <button class="btn btn-primary" type="submit">Simpan</button>
        </form>
    </div>
</div>