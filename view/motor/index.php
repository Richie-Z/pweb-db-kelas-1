<?php
include_once __DIR__ . '/../../model/Motor.php';
$listMotor = Motor::getAll();
?>
<div class="card">
    <div class="card-header">
        <h3>List Data Motor</h3>
    </div>
    <div class="card-body">
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Gambar</th>
                    <th>Plat No</th>
                    <th>Merek</th>
                    <th>Tipe</th>
                    <th>Pemilik</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $nomor = 1;
                foreach ($listMotor as $motor) : ?>
                    <tr>
                        <td><?= $nomor++ ?></td>
                        <td><img src="/images/<?= $motor->gambar ?>" class="img-thumbnail"></td>
                        <td><?= $motor->platNo ?></td>
                        <td><?= $motor->merek ?></td>
                        <td><?= $motor->tipe ?></td>
                        <td>
                            <?= $motor->mahasiswa->nama ?> /
                            <?= $motor->mahasiswa->nim ?>
                        </td>
                        <td>
                            <a class="btn btn-warning btn-sm" href="?page=ubah-motor&plat_no=<?= $motor->platNo ?>">Edit</a>
                            <a class="btn btn-danger btn-sm" href="?page=hapus-motor&id=<?= $motor->id ?>">Delete</a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>