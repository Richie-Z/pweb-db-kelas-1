<?php

include_once __DIR__ . '/../../model/Motor.php';
include_once __DIR__ . '/../../model/Upload.php';

$platNo = $_REQUEST['plat_no'];
if (!empty(Motor::getBy($platNo))) {
    echo "<h2>Plat sudah ada</h2>";
    echo "<a href='index.php'>Klik Link Ini Untuk Kembali</a>";
    die();
}
$tipe = $_REQUEST['tipe'];
$merek = $_REQUEST['merek'];
$mahasiswaNIM = $_REQUEST['mahasiswa_nim'];

$motor = new Motor();
$motor->platNo = $platNo;
$motor->tipe = $tipe;
$motor->merek = $merek;
$motor->mahasiswaNIM = $mahasiswaNIM;

$fileName = Upload::image();

if (!$fileName) {
    echo "<h2>Upload gagal</h2>";
    echo "<a href='index.php'>Klik Link Ini Untuk Kembali</a>";
    die();
}
$motor->gambar = $fileName;

$res = $motor->insert();
if ($res) {
    header('Location: /index.php?page=list-motor');
    exit();
} else {
    dd("ERROR");
}
