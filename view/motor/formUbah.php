<?php

include_once __DIR__ . '/../../model/Motor.php';
include_once __DIR__ . '/../../model/Mahasiswa.php';

$listMahasiswa = Mahasiswa::getAll();
$platNo = urldecode($_REQUEST['plat_no']);
$motor = Motor::getBy($platNo);

if ($motor === null) {
    echo "<h2>Data Motor Tidak Di Temukan</h2>";
    echo "<a href='index.php'>Klik Link Ini Untuk Kembali</a>";
    die();
}
?>
<div class="card">
    <div class="card-header">
        <h3>Ubah Motor</h3>
    </div>
    <div class="card-body">
        <form method="POST" action="view/motor/prosesUbah.php">
            <input type="hidden" name="id" value="<?= $motor->id ?>">
            <input type="hidden" name="previous_plat_no" value="<?= $motor->platNo ?>">
            <div class="form-group">
                <label>Plat no</label>
                <input required value="<?= $motor->platNo ?>" type="text" name="plat_no" class="form-control">
            </div>
            <div class="form-group">
                <label>Merek</label>
                <input required value="<?= $motor->merek ?>" type="text" name="merek" class="form-control">
            </div>
            <div class="form-group">
                <label>Tipe</label>
                <input required value="<?= $motor->tipe ?>" type="text" name="tipe" class="form-control">
            </div>
            <div class="form-group">
                <p>Pemilik</p>
                <select name="mahasiswa_nim" class="form-control">
                    <option disabled selected>Pilih Mahasiswa</option>
                    <?php foreach ($listMahasiswa as $mhs) : ?>
                        <option value="<?= $mhs->nim ?>" <?= $mhs->nim !== $motor->mahasiswaNIM ?: "selected" ?>>
                            <?= "$mhs->nim / $mhs->nama" ?>
                        </option>
                    <?php endforeach ?>
                </select>
            </div>
            <button class="btn btn-primary" type="submit">Simpan</button>
        </form>
    </div>
</div>