<?php

include_once __DIR__ . '/../../model/Motor.php';

$id = $_REQUEST['id'];
$motor = Motor::getBy($id, "id");

if ($motor === null) {
    echo "<h2>Data Motor Tidak Di Temukan</h2>";
    echo "<a href='index.php'>Klik Link Ini Untuk Kembali</a>";
    die();
}
?>
<div class="card">
    <div class="card-header">
        <h3>Anda Yakin Hapus Data Ini ?</h3>
    </div>
    <div class="card-body">
        <p>Plat No : <?= $motor->platNo ?></p>
        <p>Merek : <?= $motor->merek ?></p>
        <p>Tipe : <?= $motor->tipe ?></p>
        <p>Pemilik : <?= $motor->mahasiswa->nim ?>&nbsp;/&nbsp;<?= $motor->mahasiswa->nama ?></p>
        <a class="btn btn-warning" href="/index.php?page=list_motor">Batal</a>
        <a class="btn btn-danger" href="/view/motor/prosesHapus.php?id=<?= $motor->id ?>">Hapus</a>
    </div>
</div>