<?php

include_once __DIR__ . '/../../model/Mahasiswa.php';

$listMahasiswa = Mahasiswa::getAll();
?>
<div class="card">
    <div class="card-header">
        <h3>Tambah Motor</h3>
    </div>
    <div class="card-body">
        <form method="POST" action="view/motor/prosesTambah.php" enctype="multipart/form-data">
            <div class="form-group">
                <label>Plat no</label>
                <input type="text" name="plat_no" class="form-control">
            </div>
            <div class="form-group">
                <label>Merek</label>
                <input type="text" name="merek" class="form-control">
            </div>
            <div class="form-group">
                <label>Tipe</label>
                <input type="text" name="tipe" class="form-control">
            </div>
            <div class="form-group">
                <label>Gambar</label>
                <input type="file" name="gambar" class="form-control">
            </div>
            <div class="form-group">
                <label>Pemilik</label>
                <select name="mahasiswa_nim" class="form-control">
                    <option disabled selected>Pilih Mahasiswa</option>
                    <?php foreach ($listMahasiswa as $mhs) : ?>
                        <option value="<?= $mhs->nim ?>"><?= $mhs->nama ?></option>
                    <?php endforeach ?>
                </select>
            </div>

            <button class="btn btn-primary" type="submit">Simpan</button>
        </form>
    </div>
</div>